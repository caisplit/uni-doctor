import Vue from 'vue'
import Router from 'uni-simple-router';
Vue.use(Router);
const router = new Router({
	routes: [{
			path: "/pages/tabbar/home/home",
			name: 'tabbar-1'
		},
		{
			path: "/pages/tabbar/tabbar-2/tabbar-2",
			name: 'tabbar-2'
		},
		/* {
			path: "/pages/tabbar/tabbar-3/tabbar-3",
			name: 'tabbar-3'
		}, */
		{
			path: "/pages/tabbar/tabbar-4/tabbar-4",
			name: 'tabbar-4',
			other: {
				H5Name: ''
			},
			beforeEnter: (to, from, next) => {
				to.other.H5Name = to.query.name
				next();
			}
		},
		{
			path: "/pages/apply/apply/apply",
			name: "apply"
		},
		{
			path: "/pages/wxpay/wxpay/wxpay",
			name: "wxpay"
		},
		{
			path: "/pages/search/search",
			name: "search"
		}
	]
});

router.beforeEach((to, from, next) => {
	if (to.name == 'tabbar-5') {
		/* next({
			name: 'router4',
			params: {
				msg: '我拦截了tab5并重定向到了路由4页面上',
			},
			NAVTYPE: 'push'
		}); */
		next();
	} else {
		/* if (to.name == 'tabbar-1') {
			next({
				path: '/pages/tabbar/tabbar-2/tabbar-2',
				NAVTYPE: 'pushTab'
			});
		} else {
			next();
		} */
		next();
	}
	// console.log(to);
	// console.log(from)
})
router.afterEach((to, from) => {
	// console.log(to);
	// console.log(from)
})
// console.log(router)

export default router
