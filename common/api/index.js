/*全局api请求url*/

/*微信小程序获取用户openid*/
const POST_MP_UserId = "/wx/getMpUserInfo";

/*微信sdk地址*/
const GET_WECHAT = "/wx/getSDKInfo";

/*新增约诊*/
const POST_ADD_ORDER = "/adv/addOrder";

/*获取当前用户信息，医生或病人，根据返回的type:patient|doctor*/
const GET_USER_OPENID = "/drs/getUserInfoByOpenid";

/*根据医生查询当天的约诊列表*/
const GET_PRE_VISIT_TODAY = "/adv/addAllList";

/*根据病人的约诊列表*/
const GET_PRE_VISIT_PATIENT = "/adv/all";

/*根据医生查询当天的接诊列表*/
const GET_VISITED_TODAY = "/adv/visitAllList";

/*根据预约记录进入接诊*/
const GET_VISIT_ORDER = "/adv/visitOrder";

/*新增病人信息*/
const POST_ADD_PATIENT = "/drs/patient";

/*获取某个医生名下的病人信息列表*/
const GET_PATIENTS = "/drs/getPatients";

/*根据病人id獲取默認地址*/
const GET_DEFAULT_ADDRESS = "/drs/getDefaultAddress";

/*根据病人id獲取全部地址*/
const POST_ALL_ADDRESS = "/drs/getAllAddress";

/*上传微信临时素材*/
const PUT_UPLOAD = "/work/upload";

/*医生认证资料上传*/
const POST_DOCTOR_APPLY = "/drs/doctorApply";

/*查询药品*/
const GET_DRUG = "/drug/getDrugs";

/*新增处方*/
const POST_ADD_RECIPEL = "rec/addRecipel";

/*獲取診斷信息*/
const POST_GET_DIAGNOSIS = "rec/getDiagnosis";

/*病人提交支付申請*/
const POST_MAKEORDER = "/drs/makeOrderByDr";

/*待收费查询*/
const POST_GET_CHARGES = "/drs/getAllPreCharges";

/*待发药查询*/
const POST_GET_SEND = "/drs/getAllPreSends";

/*待发药查询*/
const POST_PROFIT = "/drs/doctorProfit";

export default {
    GET_WECHAT,
    GET_USER_OPENID,
    GET_PRE_VISIT_PATIENT,
    GET_PRE_VISIT_TODAY,
    GET_VISITED_TODAY,
    GET_VISIT_ORDER,
    GET_PATIENTS,
    GET_DEFAULT_ADDRESS,
    GET_DRUG,

    POST_ADD_ORDER,
    POST_ADD_PATIENT,
    POST_DOCTOR_APPLY,
    POST_ADD_RECIPEL,
    POST_GET_DIAGNOSIS,
    POST_ALL_ADDRESS,
    POST_MAKEORDER,
    POST_GET_CHARGES,
    POST_GET_SEND,

    POST_PROFIT,

    PUT_UPLOAD,
}
