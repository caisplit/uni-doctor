import Vue from 'vue'
import App from './App'
import store from './store'

import router from './common/uni-app-router/useRouter'
import apiUrl from './common/api'
import http from './common/http'

Vue.prototype.$store = store

Vue.prototype.API = http;
Vue.prototype.ApiUrl = apiUrl;

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App,
	router,
	store
})
app.$mount()

// 纯粹方便浏览器调试
// #ifdef H5 
Window.prototype.vm = app
// #endif
